/**
 * работа с формой
 * @param options
 */
var form = function (options) {
    var
        /**
         * кнопка отправки формы
         * @type {jQuery}
         */
        $submit = jQuery('.js-form-submit'),

        /**
         * инпуты формы
         * @type {jQuery}
         */
        $inputs = jQuery('.js-form-input'),

        /**
         * нода показа ошибок
         * @type {jQuery}
         */
        $error = jQuery('.js-form-error'),

        /**
         * тут текст ошибок
         */
        errorText,

        /**
         * заполненные поля формы
         * @type {object}
         */
        fields,

        /**
         * необходимые поля
         * @type {[]}
         */
        required = ['email'],

        /**
         * ошибки формы
         * @type {{email: string}}
         */
        errors = {
            email: 'Необходимо заполнить поле e-mail.'
        },

        /**
         * валидация формы
         * возвращает имена полей с неправильными данными
         * @returns {Array}
         */
        validate = function() {
            fields = jQuery('form').serializeArray();

            /**
             * функции валидации полей формы по input name
             * @type {{email: Function}}
             */
            var validators = {
                /**
                 * валидация строки на соответствие email
                 * @param {string} str
                 * @return {boolean}
                 */
                email: function (str) {
                    return !!/^[a-z0-9+._\-]+@([a-z0-9\-]+\.)+[a-z]{2,6}$/.exec(str);
                }
            };
            return jQuery(fields).map(function(index, field) {
                if (~(required.indexOf(field.name))) {
                    return validators[field.name] && !validators[field.name](field.value) ? field.name : undefined;
                }
            })
        };

    $inputs.focus(function() {
        $submit.removeClass('disabled');
        $error.html('').hide();
        $inputs.removeClass('error')
    });

    $submit.click(function () {
        if ($submit.is('.disabled')) {
            return false;
        }
        var findErrors = validate();
        if (!findErrors.length) {
//                jQuery.ajax({
//                    url: formUrl,
//                    method: 'post',
//                    data: fields,
//                    success: function() {
//                        (new modal({
//                            selector: '.js-modal-done'
//                        })).show()
//                    }
//                });

            (new modal({
                selector: '.js-modal-done'
            })).show()
        } else {
            $submit.addClass('disabled');
            errorText = Array.prototype.join.apply(jQuery(findErrors).map(function(index, error) {
                jQuery('[name="' + error + '"]').addClass('error');
                return errors[error];
            }));
            $error.html(errorText).show();
        }
    });
};
